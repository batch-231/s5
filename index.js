class Product {
    constructor(name, price) {
        this.name = name;
        this.price = price;
        this.isActive = true;
    }

    archive() {
        if (this.isActive === false) {
            console.log(`[ERROR]: Product ${this.name} is already archived.`);
            return this;
        }

        this.isActive = false;
        console.log(`[SUCCESS]: Product ${this.name} successfully archived.`);
        return this;
    }

    // Added method
    unArchive() {
        if (this.isActive === true) {
            console.log(`[ERROR]: Product ${this.name} is already active.`);
        }

        this.isActive = true;
        console.log(`[SUCCESS]: Product ${this.name} successfully unarchived.`);
        return this;
    }

    updatePrice(newPrice) {
        if (typeof newPrice !== "number") {
            console.log(`[ERROR]: Product price updated failed. Please make sure to input number data type only.`);
            return this;
        }

        console.log(`[SUCCESS]: Product ${this.name} price successfully updated. ${this.price} -> ${newPrice}`);
        this.price = newPrice;
        return this;
    }
};


class Cart {
    constructor() {
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, number) {
        if (typeof product !== "object" || typeof number !== "number") {
            console.log(`[ERROR]: Invalid data type arguments.`);
            return this;
        }

        if (product.isActive === false) {
            console.log(`[ERROR]: The product you are trying to add is currently archived.`);
            return this;
        }

        this.contents.push({
            product,
            quantity: number
        });

        console.log(`[SUCCESS]: Product: ${product.name} with a quantity of ${number} successfully added to cart!`);
        return this;
    }

    showCartContents() {
        this.contents.length === 0
            ? console.log(`[NOTICE]: Your cart is still empty.`)
            : console.log(this.contents)

        return this;
    }

    updateProductsQuantity(productName,  newQty) {
        if (typeof productName !== "string" || typeof newQty !== "number") {
            console.log(`[ERROR]: Invalid data type arguments.`);
            return this;
        }

        let productExists = false;

        this.contents.forEach(product => {
            if (product.product.name.toLowerCase() === productName.toLowerCase()) {
                product.quantity = newQty;
                productExists = true;
            }
        })

        productExists
            ? console.log(`[SUCCESS]: Product ${productName}'s new quantity is now ${newQty}.`)
            : console.log(`[ERROR]: "${productName}" doesn't exist in your cart`)

        return this;
    }

    clearCartContents() {
        this.contents.length === 0
            ? console.log(`[NOTICE]: Your cart is still empty. Nothing to clear.`)
            : console.log(`[SUCESS] Your cart is now empty.`)

        this.contents = [];
        this.totalAmount = 0;
        return this;
    }

    computeTotal() {
        if (this.contents.length === 0) {
            console.log(`[NOTICE]: Your cart is still empty. No totalAmount to be computed`);
            return this;
        }

        let total = 0;

        this.contents.forEach(product => {
            total += product.product.price * product.quantity;
        });
        
        this.totalAmount = total;
        console.log(`[SUCCESS]: Total Amount: ${this.totalAmount}`);
        return this;
    }
};

class Customer {
    constructor(email) {
        this.email = email;
        this.cart = new Cart();
        this.orders = [];
    }

    checkOut() {
        if (this.cart.contents.length === 0) {
            console.log(`[NOTICE]: Your cart is still empty. Nothing to checkoout`);
            return this;
        }

        this.orders.push({
            products: this.cart.contents,
            totalAmount: this.cart.computeTotal().totalAmount
        });

        console.log(`[SUCCESS]: Checkout success!`);
        this.cart.clearCartContents(); // Clear this customers cart contents upon checking out
        console.log(`\n### [ORDER DETAILS] ###`);

        // The [this.orders.length - 1] will make sure that when a Customer checks out, the latest addition in the orders property will be the one to be logged in the console.
        this.orders[this.orders.length - 1].products.forEach(product => {
            console.log(`\nProduct Name: ${product.product.name}\n\tProduct Price: ${product.product.price}\n\tQty:${product.quantity}\n\n`);
        });

        console.log(`Total Amount: ${this.orders[this.orders.length - 1].totalAmount}\n\n`);

        return this;
    }
};



// ################# [TEST STATEMENTS] #################
// Customer instantiation
const pat = new Customer('patrick@mail.com');

// Product instantiations
const prodA = new Product('PTRCK Fire Tee', 1000);
const prodB = new Product('PTRCK Water Tee', 1000);
const prodC = new Product('PTRCK Air Tee', 1000);
const prodD = new Product('PTRCK Elemental Hoodie', 2500);

// Archiving and Udpating a Product
prodC.archive(); // success: archiving prodC
prodC.archive(); // error: re-archiving prodC
pat.cart.addToCart(prodC, 20); // error: adding prodC (prodC is archived)
prodD.updatePrice(3500); // success: price updated
prodD.updatePrice("1000"); // error: invalid arguments

// Adding products to your cart
pat.cart.addToCart(prodA, 1); // success
pat.cart.addToCart(prodD, 1); // success
pat.cart.addToCart("product", "32"); // error: invalid data type

// Updating a product quantity in your cart
pat.cart.updateProductsQuantity("PTRCK Fire Tee", 3); // success
pat.cart.updateProductsQuantity("Do not exist", 100) // error - product that does not exist
pat.cart.updateProductsQuantity("PTRCK Fire Tee", "23") // error - invalid arguments
pat.cart.showCartContents();
// pat.cart.computeTotal(); // no need to call this because this method is also being called inside the checkout method of the user
pat.checkOut();

// Unarchiving the archived product in line 169
// Adding it to the cart
// Check out the new cart after checking out the previous one.
// Let's see if the products in the orders will only log the new cart that has been checked out.
prodC.unArchive();
pat.cart.addToCart(prodC, 20);
pat.cart.showCartContents();

pat.checkOut();
pat.checkOut(); // error: check out again if cart is now empty
pat.cart.computeTotal();  // error: cart is till empty
pat.cart.showCartContents(); // error: empty cart





